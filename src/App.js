import React from 'react'
import { Route } from 'react-router-dom';

import EnterRoom from './components/EnterRoom/EnterRoom';
import ChatScreen from './components/ChatScreen/ChatScreen';
import ThemeSwitch from './components/ThemeSwitch';


const App = () => {
  return (
    <>
      <ThemeSwitch />
      <Route path='/' exact component={EnterRoom} />
      <Route path='/chat' component={ChatScreen} />
    </>
  );
}


export default App;
