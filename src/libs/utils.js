export const hyphenateString = (string) => {
  return string.replaceAll(' ', '-');
}
