import React from 'react';
import { Flex, Text, Code } from '@chakra-ui/layout';
import Avatar from 'boring-avatars';

import { avatarVariant, avatarColors } from '../../libs/constants';

const MessagesContainer = (props) => {
  const { messages, name, avatar } = props;

  if (!messages) return <Text fontSize="xl">(xl)No conversation!</Text>


  const renderMessages = () => {
    return messages.map((item, index) => {
      if (item.user === 'admin') {
        return(
          <Flex key={`${item.user}-${index}`} padding="1.5" justify="center">
            <Code max-width="50%" colorScheme="red" children={item.text} />
          </Flex>
        );
      }

      if (name.toLowerCase() === item.user.toLowerCase()) {
        return (
          <Flex key={`${item.user}-${index}`} padding="1.5" justify="flex-end">
            <Code max-width="70%" marginRight="1" fontSize={[null, "sm", null, null, "xl"]} colorScheme="whatsapp" children={item.text} />
            <Avatar
              size={20}
              name={avatar.replaceAll('-', ' ')}
              variant={avatarVariant}
              colors={avatarColors}
            />
          </Flex>
        );
      }

      return (
        <Flex key={`${item.user}-${index}`} padding="1.5">
          <Avatar
            size={20}
            name={item.avatar.replaceAll('-', ' ')}
            variant={avatarVariant}
            colors={avatarColors}
          />
          <Code max-width="70%" marginLeft="1" fontSize={[null, "sm", null, null, "xl"]} colorScheme="messenger" children={item.text} />
        </Flex>
      );
    });
  }

  return (
    <Flex  flexDirection="column">
      {renderMessages()}
    </Flex>
  );
}

export default MessagesContainer;
