import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from "socket.io-client";

import { Flex, Input, InputGroup, InputRightElement, IconButton } from '@chakra-ui/react';

import MessagesContainer from '../MessagesContainer/MessagesContainer';

import { RiSendPlaneFill } from "react-icons/ri";

let socket;

const ChatScreen = ({ location }) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [avatar, setAvatar] = useState('');

  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  // NOTE - Uncomment this and run backend locally for dev purposes
  // const ENDPOINT = 'localhost:5000';
  const ENDPOINT = 'https://aqueous-gorge-17058.herokuapp.com';

  useEffect(() => {
    const { name, room, a } = queryString.parse(location.search);

    socket = io(ENDPOINT, { transports : ['websocket'] });

    setRoom(room);
    setName(name);
    setAvatar(a);

    socket.emit('join', { name, room, avatar: a }, (error) => {
      if(error) {
        alert(error);
      }
    });

    // cleanup
    return () => {
      socket.emit('disconnect');

      socket.off();
    }
  }, [ENDPOINT, location.search]);

  useEffect(() => {
    socket.on('message', (message) => {
      setMessages([...messages, message]);
    });
  }, [messages]);

  const sendMessage = (event) => {
    event.preventDefault();

    if (message) {
      socket.emit('sendMessage', message, () => setMessage(''));
    }
  }

  // console.log('MSG =', message, "MSGS =>", messages, '\nsocket', socket);

  return (
    <Flex flexDirection="column" height="calc(100vh - var(--chakra-sizes-10))">
      <MessagesContainer messages={messages} name={name} avatar={avatar} room={room} />

      <Flex position="absolute" bottom="0.5" width="full">
        <InputGroup>
          <Input
            placeholder="Type your message..."
            value={message}
            onChange={(event) => setMessage(event.target.value)}
            onKeyPress={(event) => event.key === 'Enter' ? sendMessage(event) : null }
          />

          <InputRightElement width="4.5rem">
            <IconButton
              colorScheme="teal"
              aria-label="Send"
              size={['sm']}
              icon={<RiSendPlaneFill />}
              onClick={sendMessage}
            />
          </InputRightElement>

        </InputGroup>
      </Flex>
    </Flex>
  );
}

export default ChatScreen;
