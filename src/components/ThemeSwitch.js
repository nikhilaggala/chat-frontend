import React from 'react';
import { RiSunLine, RiMoonFill } from "react-icons/ri";

import {
  IconButton,
  useColorMode
} from '@chakra-ui/react';


const ThemeSwitch = () => {
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <IconButton aria-label="toggle-theme" icon={colorMode === 'dark' ? <RiSunLine /> : <RiMoonFill /> } onClick={toggleColorMode} />
  );
}


export default ThemeSwitch;
