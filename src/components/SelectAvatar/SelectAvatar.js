import React from 'react';
import Avatar from 'boring-avatars';
import { Flex, Box, Text } from '@chakra-ui/layout';

import { hyphenateString } from '../../libs/utils';
import { avatarVariant, avatarColors } from '../../libs/constants';


const SelectAvatar = (props) => {
  const { setSelectedAvatar, selectedAvatar  } = props;

  const avatarNames = ['Mary Baker', 'Amelia Earhart', 'Annie Dodge', 'Phillis Wheatley'];

  const renderAvatars = () => {
    return avatarNames.map((item, index) => {
      return (
        <Box
          key={index}
          marginX="1.5"
          padding="1"
          border={hyphenateString(item) === hyphenateString(selectedAvatar) ? "2px solid" : 'none'}
          borderColor={hyphenateString(item) === hyphenateString(selectedAvatar) ? "blue.500" : ''}
          borderRadius="40px"
          onClick={() => setSelectedAvatar(item)}
        >
          <Avatar
            size={40}
            name={item}
            variant={avatarVariant}
            colors={avatarColors}
          />
        </Box>
      );
    });
  }

  return (
    <Flex flexDirection="column" justifyContent="center" align="center">
      <Text fontSize={[null, "sm", null, null, "xl"]}>Choose your avatar!</Text>
      <Flex marginY="1.5">
        {renderAvatars()}
      </Flex>
    </Flex>
  );
}


export default SelectAvatar;
