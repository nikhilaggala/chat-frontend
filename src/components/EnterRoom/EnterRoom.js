
import React, { useState } from 'react';
import { Link } from "react-router-dom";

import {
  Flex,
  FormControl,
  FormLabel,

  Input,
  Button,
} from '@chakra-ui/react';

import SelectAvatar from '../SelectAvatar/SelectAvatar';
import { hyphenateString } from '../../libs/utils';


const EnterRoom = () => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [invalid, setInvalid] = useState(false);
  const [selectedAvatar, setSelectedAvatar ] = useState('Mary Baker');

  function onEnterRoomClick(e) {
    if (!room || !name) {
      setInvalid(true);
      e.preventDefault();
    }
  }

  return (
    <Flex justify="center" align="center" flexDirection="column" marginTop="32">
      <SelectAvatar setSelectedAvatar={setSelectedAvatar} selectedAvatar={selectedAvatar} />

      <FormControl id="user-name" isRequired width={["xs","sm","md"]} marginBottom="3">
        <FormLabel>Enter your name</FormLabel>
        <Input
          placeholder="Name"
          isInvalid={invalid && !name}
          errorBorderColor="crimson"
          onChange={(event) => setName(event.target.value)}
        />
      </FormControl>


      <FormControl id="room-id" isRequired width={["xs","sm","md"]} marginBottom="5">
        <FormLabel>Enter room id</FormLabel>
        <Input
          placeholder="Room id"
          isInvalid={invalid && !room}
          errorBorderColor="crimson"
          onChange={(event) => setRoom(event.target.value)}
        />
      </FormControl>

      <Link onClick={onEnterRoomClick} to={`/chat?name=${name}&room=${room}&a=${hyphenateString(selectedAvatar)}`}>
        <Button size="lg" colorScheme="teal"> {`Enter Room :)`}</Button>
      </Link>

    </Flex>
  );
}


export default EnterRoom;
