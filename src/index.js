import React from 'react'
import ReactDOM from 'react-dom'
import { ColorModeScript, ChakraProvider, theme } from "@chakra-ui/react"
import { BrowserRouter as Router } from 'react-router-dom';

// import { theme } from "./themes"
// import  * as cssVariables from './themes/cssVariables';

import App from './App';

// console.log('cssVariablescssVariablescssVariablescssVariables', theme);


ReactDOM.render(
  <>
    <ColorModeScript initialColorMode={theme.config.initialColorMode} />
    <ChakraProvider theme={theme}>
      <Router>
        <App />
      </Router>
    </ChakraProvider>
  </>,
  document.querySelector('#root')
);
