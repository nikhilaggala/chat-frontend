# About

This project is a simple chat screen with [CRA](https://create-react-app.dev/) which use a Socket.io to communicate.


## Packages used in this project\

- [Chakra UI](https://chakra-ui.com/) - For all the Elements.
- [BooringAvatars](https://www.npmjs.com/package/boring-avatars) - For avatars.

## Backend used - [Chat Backend](https://gitlab.com/nikhilaggala/chat-backend)


## To start this project -

- Clone this into your directory

### yarn && yarn start
